import pathlib
import typing
from unittest.mock import MagicMock
from unittest.mock import patch

import aok
import lobotomy
import yaml
from pytest import mark

from manager import _allocator
from manager import _types

_DIRECTORY = pathlib.Path(__file__).resolve().parent.joinpath("scenarios")
_FILENAMES = [p.name for p in _DIRECTORY.iterdir() if p.name.endswith(".yaml")]


def _to_dictable(raw: typing.Dict[str, typing.Any]) -> MagicMock:
    """Create mock kubernetes API object that can be converted to the specified dict."""
    out = MagicMock()
    out.to_dict.return_value = raw
    return out


@mark.parametrize("filename", _FILENAMES)
@lobotomy.patch()
@patch("manager._controller._pods.core_v1.Pod.get_resource_api")
@patch("manager._controller._nodes.core_v1.Node.get_resource_api")
def test_allocation_scenarios(
    node_get_resource_api: MagicMock,
    pod_get_resource_api: MagicMock,
    lobotomized: lobotomy.Lobotomy,
    filename: str,
):
    """Should allocate..."""
    scenario = yaml.full_load(_DIRECTORY.joinpath(filename).read_text())
    lobotomized.add_call(
        "ec2", "describe_fleets", {"Fleets": scenario["described_fleets"]}
    )
    lobotomized.add_call(
        "ec2",
        "describe_fleet_instances",
        {"ActiveInstances": scenario["described_fleet_instances"]},
    )

    listed_pods = MagicMock()
    pod_api = MagicMock()
    pod_api.list_pod_for_all_namespaces.return_value = listed_pods
    pod_get_resource_api.return_value = pod_api
    listed_pods.items = [_to_dictable(p) for p in scenario.get("pods", [])]

    listed_nodes = MagicMock()
    node_api = MagicMock()
    node_api.list_node.return_value = listed_nodes
    node_get_resource_api.return_value = node_api
    listed_nodes.items = [_to_dictable(n) for n in scenario.get("nodes", [])]

    configs = _types.ManagerConfigs().parse({}, scenario["configs"])

    observed = _allocator.get_capacity_targets(configs)
    print(f"\n=== OBSERVED ===\n{yaml.dump(observed)}\n")

    expected: aok.Okay = scenario["expected"]
    expected.assert_all(observed)
